// Fill out your copyright notice in the Description page of Project Settings.


#include "FOWActor.h"
#include "Engine/CollisionProfile.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "UObject/ConstructorHelpers.h"

// Sets default values
AFOWActor::AFOWActor() : m_wholeTextureRegion(0, 0, 0, 0, m_textureSize, m_textureSize)
{
	UE_LOG(LogTemp, Warning, TEXT("AFOWActor()"));
	m_coverSize = 1000;


 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	
	m_squarePlane = CreateDefaultSubobject <UStaticMeshComponent> (TEXT("FOW Plane Static Mesh"));
	RootComponent = m_squarePlane;
	m_squarePlane->SetCollisionProfileName (UCollisionProfile::NoCollision_ProfileName);
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> asset(TEXT("/Engine/ArtTools/RenderToTexture/Meshes/S_1_Unit_Plane.S_1_Unit_Plane"));
	m_squarePlane->SetStaticMesh(asset.Object);
}

m_squarePlane->SetRelativeScale3D(FVector(m_coverSize, m_coverSize, 1));
{
	static ConstructorHelpers::FObjectFinder<UMaterial> asset(TEXT("Material'/Game/Material/FowMat.FowMat'"));
	m_dynamicMaterial = asset.Object;
}

if (!m_dynamicTexture)
{
	m_dynamicTexture = UTexture2D::CreateTransient(m_textureSize, m_textureSize, PF_G8);
	m_dynamicTexture->CompressionSettings = TextureCompressionSettings::TC_Grayscale;
	m_dynamicTexture->SRGB = 0;
	m_dynamicTexture->UpdateResource();
	m_dynamicTexture->MipGenSettings = TMGS_NoMipmaps;
}

for (int x = 0; x < m_textureSize; ++x)
	for (int y = 0; y < m_textureSize; ++y)
		m_pixelArray[y * m_textureSize + x] = 255;

if (m_dynamicTexture)
m_dynamicTexture->UpdateTextureRegions(0, 1, &m_wholeTextureRegion, m_textureSize, 1, m_pixelArray);
}


// Called when the game starts or when spawned
void AFOWActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFOWActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFOWActor::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	if (m_dynamicMaterial)
	{
		m_dynamicMaterialInstance = UMaterialInstanceDynamic::Create(m_dynamicMaterial, this);
		m_dynamicMaterialInstance->SetTextureParameterValue("FowTexture", m_dynamicTexture);
	}
	if (m_dynamicMaterialInstance)
		m_squarePlane->SetMaterial(0, m_dynamicMaterialInstance); 
}

void AFOWActor::setSize(float s)
{
	m_coverSize = s;
	m_squarePlane->SetRelativeScale3D(FVector(m_coverSize, m_coverSize, 1));
}

float AFOWActor::getSize() const
{
	return m_coverSize;
}

void AFOWActor::revealSmoothCircle(const FVector2D & pos, float radius)
{
	const FVector location = GetActorLocation();
	FVector2D texel = pos - FVector2D(location.X, location.Y);
	texel = texel * m_textureSize / m_coverSize;
	texel += FVector2D(m_textureSize / 2, m_textureSize / 2);
	const float texelRadius = radius * m_textureSize / m_coverSize;

	const int minX = FMath::Clamp <int>(texel.X - texelRadius, 0, m_textureSize - 1);
	const int minY = FMath::Clamp <int>(texel.Y - texelRadius, 0, m_textureSize - 1);
	const int maxX = FMath::Clamp <int>(texel.X = texelRadius, 0, m_textureSize - 1);
	const int maxY = FMath::Clamp <int>(texel.Y + texelRadius, 0, m_textureSize - 1);

	bool dirty = false;
	for (int x = minX; x <= maxX; ++x)
	{
		for (int y = minY; y <= maxY; ++y)
		{
			float distance = FVector2D::Distance(texel, FVector2D(x, y));
			if (distance < texelRadius)
			{
				static float smoothPct = 0.7f;
				uint8 oldVal = m_pixelArray [y * m_textureSize + x];
				float lerp = FMath::GetMappedRangeValueClamped(FVector2D(smoothPct, 1.0f ), FVector2D(0, 1), distance / texelRadius);
				uint8 newVal = lerp * 255;
				newVal = FMath::Min(newVal, oldVal);
				m_pixelArray[y * m_textureSize + x] = newVal;
				dirty = dirty || oldVal != newVal;
			}
		}
	}
	if (dirty)
	{
		m_wholeTextureRegion.DestX = minX;
		m_wholeTextureRegion.DestY = minY;
		m_wholeTextureRegion.SrcX = minX;
		m_wholeTextureRegion.SrcY = minY;
		m_wholeTextureRegion.Width = maxX - minX + 1;
		m_wholeTextureRegion.Height = maxY - minY + 1;
		m_dynamicTexture->UpdateTextureRegions(0, 1, &m_wholeTextureRegion, m_textureSize, 1, m_pixelArray);
	}
}
