// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Paintball_Lewis2/Paintball_Lewis2HUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePaintball_Lewis2HUD() {}
// Cross Module References
	PAINTBALL_LEWIS2_API UClass* Z_Construct_UClass_APaintball_Lewis2HUD_NoRegister();
	PAINTBALL_LEWIS2_API UClass* Z_Construct_UClass_APaintball_Lewis2HUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_Paintball_Lewis2();
// End Cross Module References
	void APaintball_Lewis2HUD::StaticRegisterNativesAPaintball_Lewis2HUD()
	{
	}
	UClass* Z_Construct_UClass_APaintball_Lewis2HUD_NoRegister()
	{
		return APaintball_Lewis2HUD::StaticClass();
	}
	struct Z_Construct_UClass_APaintball_Lewis2HUD_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APaintball_Lewis2HUD_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AHUD,
		(UObject* (*)())Z_Construct_UPackage__Script_Paintball_Lewis2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APaintball_Lewis2HUD_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Rendering Actor Input Replication" },
		{ "IncludePath", "Paintball_Lewis2HUD.h" },
		{ "ModuleRelativePath", "Paintball_Lewis2HUD.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APaintball_Lewis2HUD_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APaintball_Lewis2HUD>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APaintball_Lewis2HUD_Statics::ClassParams = {
		&APaintball_Lewis2HUD::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002ACu,
		METADATA_PARAMS(Z_Construct_UClass_APaintball_Lewis2HUD_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APaintball_Lewis2HUD_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APaintball_Lewis2HUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APaintball_Lewis2HUD_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APaintball_Lewis2HUD, 2263012642);
	template<> PAINTBALL_LEWIS2_API UClass* StaticClass<APaintball_Lewis2HUD>()
	{
		return APaintball_Lewis2HUD::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APaintball_Lewis2HUD(Z_Construct_UClass_APaintball_Lewis2HUD, &APaintball_Lewis2HUD::StaticClass, TEXT("/Script/Paintball_Lewis2"), TEXT("APaintball_Lewis2HUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APaintball_Lewis2HUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
