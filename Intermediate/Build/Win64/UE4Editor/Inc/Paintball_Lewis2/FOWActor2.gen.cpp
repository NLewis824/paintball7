// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Paintball_Lewis2/FOWActor2.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFOWActor2() {}
// Cross Module References
	PAINTBALL_LEWIS2_API UClass* Z_Construct_UClass_AFOWActor2_NoRegister();
	PAINTBALL_LEWIS2_API UClass* Z_Construct_UClass_AFOWActor2();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Paintball_Lewis2();
// End Cross Module References
	void AFOWActor2::StaticRegisterNativesAFOWActor2()
	{
	}
	UClass* Z_Construct_UClass_AFOWActor2_NoRegister()
	{
		return AFOWActor2::StaticClass();
	}
	struct Z_Construct_UClass_AFOWActor2_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AFOWActor2_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Paintball_Lewis2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFOWActor2_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FOWActor2.h" },
		{ "ModuleRelativePath", "FOWActor2.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AFOWActor2_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AFOWActor2>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AFOWActor2_Statics::ClassParams = {
		&AFOWActor2::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AFOWActor2_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AFOWActor2_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AFOWActor2()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AFOWActor2_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AFOWActor2, 1425917200);
	template<> PAINTBALL_LEWIS2_API UClass* StaticClass<AFOWActor2>()
	{
		return AFOWActor2::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AFOWActor2(Z_Construct_UClass_AFOWActor2, &AFOWActor2::StaticClass, TEXT("/Script/Paintball_Lewis2"), TEXT("AFOWActor2"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AFOWActor2);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
