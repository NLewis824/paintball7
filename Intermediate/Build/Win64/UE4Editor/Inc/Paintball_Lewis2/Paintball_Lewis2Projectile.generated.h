// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef PAINTBALL_LEWIS2_Paintball_Lewis2Projectile_generated_h
#error "Paintball_Lewis2Projectile.generated.h already included, missing '#pragma once' in Paintball_Lewis2Projectile.h"
#endif
#define PAINTBALL_LEWIS2_Paintball_Lewis2Projectile_generated_h

#define paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Projectile_h_12_SPARSE_DATA
#define paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Projectile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Projectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Projectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintball_Lewis2Projectile(); \
	friend struct Z_Construct_UClass_APaintball_Lewis2Projectile_Statics; \
public: \
	DECLARE_CLASS(APaintball_Lewis2Projectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paintball_Lewis2"), NO_API) \
	DECLARE_SERIALIZER(APaintball_Lewis2Projectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Projectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPaintball_Lewis2Projectile(); \
	friend struct Z_Construct_UClass_APaintball_Lewis2Projectile_Statics; \
public: \
	DECLARE_CLASS(APaintball_Lewis2Projectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paintball_Lewis2"), NO_API) \
	DECLARE_SERIALIZER(APaintball_Lewis2Projectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Projectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APaintball_Lewis2Projectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintball_Lewis2Projectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_Lewis2Projectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_Lewis2Projectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_Lewis2Projectile(APaintball_Lewis2Projectile&&); \
	NO_API APaintball_Lewis2Projectile(const APaintball_Lewis2Projectile&); \
public:


#define paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Projectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_Lewis2Projectile(APaintball_Lewis2Projectile&&); \
	NO_API APaintball_Lewis2Projectile(const APaintball_Lewis2Projectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_Lewis2Projectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_Lewis2Projectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintball_Lewis2Projectile)


#define paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Projectile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(APaintball_Lewis2Projectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(APaintball_Lewis2Projectile, ProjectileMovement); }


#define paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Projectile_h_9_PROLOG
#define paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Projectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Projectile_h_12_PRIVATE_PROPERTY_OFFSET \
	paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Projectile_h_12_SPARSE_DATA \
	paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Projectile_h_12_RPC_WRAPPERS \
	paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Projectile_h_12_INCLASS \
	paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Projectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Projectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Projectile_h_12_PRIVATE_PROPERTY_OFFSET \
	paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Projectile_h_12_SPARSE_DATA \
	paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Projectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Projectile_h_12_INCLASS_NO_PURE_DECLS \
	paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Projectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTBALL_LEWIS2_API UClass* StaticClass<class APaintball_Lewis2Projectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Projectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
