// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_LEWIS2_Paintball_Lewis2Character_generated_h
#error "Paintball_Lewis2Character.generated.h already included, missing '#pragma once' in Paintball_Lewis2Character.h"
#endif
#define PAINTBALL_LEWIS2_Paintball_Lewis2Character_generated_h

#define paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Character_h_14_SPARSE_DATA
#define paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Character_h_14_RPC_WRAPPERS
#define paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Character_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Character_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintball_Lewis2Character(); \
	friend struct Z_Construct_UClass_APaintball_Lewis2Character_Statics; \
public: \
	DECLARE_CLASS(APaintball_Lewis2Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paintball_Lewis2"), NO_API) \
	DECLARE_SERIALIZER(APaintball_Lewis2Character)


#define paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Character_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAPaintball_Lewis2Character(); \
	friend struct Z_Construct_UClass_APaintball_Lewis2Character_Statics; \
public: \
	DECLARE_CLASS(APaintball_Lewis2Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paintball_Lewis2"), NO_API) \
	DECLARE_SERIALIZER(APaintball_Lewis2Character)


#define paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Character_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APaintball_Lewis2Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintball_Lewis2Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_Lewis2Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_Lewis2Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_Lewis2Character(APaintball_Lewis2Character&&); \
	NO_API APaintball_Lewis2Character(const APaintball_Lewis2Character&); \
public:


#define paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Character_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_Lewis2Character(APaintball_Lewis2Character&&); \
	NO_API APaintball_Lewis2Character(const APaintball_Lewis2Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_Lewis2Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_Lewis2Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintball_Lewis2Character)


#define paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Character_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(APaintball_Lewis2Character, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(APaintball_Lewis2Character, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(APaintball_Lewis2Character, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(APaintball_Lewis2Character, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(APaintball_Lewis2Character, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(APaintball_Lewis2Character, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(APaintball_Lewis2Character, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(APaintball_Lewis2Character, L_MotionController); }


#define paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Character_h_11_PROLOG
#define paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Character_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Character_h_14_PRIVATE_PROPERTY_OFFSET \
	paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Character_h_14_SPARSE_DATA \
	paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Character_h_14_RPC_WRAPPERS \
	paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Character_h_14_INCLASS \
	paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Character_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Character_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Character_h_14_PRIVATE_PROPERTY_OFFSET \
	paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Character_h_14_SPARSE_DATA \
	paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Character_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Character_h_14_INCLASS_NO_PURE_DECLS \
	paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Character_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTBALL_LEWIS2_API UClass* StaticClass<class APaintball_Lewis2Character>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID paintball4_Source_Paintball_Lewis2_Paintball_Lewis2Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
